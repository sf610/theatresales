/*--------------------------------------------------------------------
 * Programming assignment 4 & 5 --- A theatre seat POS program
 * Steve Froehlich    CSCI 1125   Spring 2013   Prof Wrenn
 * Program description:
 * Displays an array of button representing seats. User clicks to select
 * seats and purchases and needed.
 * Global Variables: rows, cols can be changed to reflect number of
 * rows and seats
 * Note: once a seat or seats are purchased refunds are not allowed
 ----------------------------------------------------------------------*/


import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import java.io.*;
import java.util.*;

public class TheatreSeatSales{
	
	//global variables
	//static boolean taken;
	static int rows = 15;  //total number of rows seating
	static int cols = 30;  //total number of seats per row
	int currRow, currSeat;  //current seat for locating seat number
	int totalSoldSeats = 0;  //keeps track of total sold seats
	int currSoldSeats = 0;   //increments every purchase and resets after display
	
	static double totalSales = 0;
	static double currentSale;
	
	//array of prices
	static double [] prices = new double[rows];
	//array of prices for output
	static String [] pricesOut = new String[rows];
	//ArrayList for held seats. this is cleared after seats are selected and purchased
	ArrayList<Integer> heldSeats = new ArrayList<Integer>();
	//keeps track of which row seats have been purchased in
	ArrayList<Integer> boughtRow = new ArrayList<Integer>();
	
	
	//text fields for seat input
	JTextField userRowText = new JTextField();
	JTextField userSeatText = new JTextField();
	
	//green and red seat Image icons
	ImageIcon green = new ImageIcon("seat_green.jpg");
	ImageIcon red = new ImageIcon("seat_red.jpg");
	
	//green and red buttons for swapping
	final JButton available = new JButton(green);
	final JButton reserved = new JButton(red);
	
	//label updates when a seat is sold or cancelled
	JLabel soldSeats = new JLabel("Seats Sold:");
	JLabel availSeats = new JLabel("Available Seats: ");
	JLabel currSale = new JLabel("Current Sale:");
	JLabel currRowSeat = new JLabel("Row:  Seat:");
	JLabel userRow = new JLabel("Row: ");
	JLabel userSeat = new JLabel("Seat: ");
	
	public TheatreSeatSales(){
		
		//main frame
		final JFrame j = new JFrame("Theatre Sales");
		j.setLayout(new BorderLayout());
		
		//panels for the frame
		JPanel right = new JPanel();
		JPanel center = new JPanel();
		JPanel selectPurchase = new JPanel();
		
		//operation buttons
		JButton purchaseBtn = new JButton("Purchase Selected Seats");
		JButton userRowColBtn = new JButton("Purchase");
		
		//menu items
		JMenuBar menuBar = new JMenuBar();
		
		JMenu mMenu = new JMenu("Menu");
		JMenuItem mReset = new JMenuItem("Reset all sales");
		JMenuItem mExit = new JMenuItem("Exit");
		mMenu.add(mReset);
		mMenu.add(mExit);
		
		menuBar.add(mMenu);
		
		JMenu mOptions = new JMenu("Options");
		JMenuItem mViewSales = new JMenuItem("View Total Sales");
		JMenuItem mPurchaseRow = new JMenuItem("Purchase Row");
		JMenuItem mAbout = new JMenuItem("About");
	
		mOptions.add(mViewSales);
		mOptions.add(mPurchaseRow);
		mOptions.add(mAbout);
		
		menuBar.add(mOptions);
		//end action listeners
		
		//array of buttons representing seats
		final JButton[][] seats = new JButton[rows][cols]; 
		for(int r = 0; r < rows; r++){
			for(int c = 0; c < cols; c++){
				seats[r][c] = new JButton(green);
				seats[r][c].addActionListener(new MyActionListener());
				seats[r][c].setActionCommand("Row: " + (r+1) + " Seat: " + (c+1)); //set location of seat
				seats[r][c].putClientProperty("Row", (r+1));
				seats[r][c].putClientProperty("Seat", (c+1));
				seats[r][c].setToolTipText("Row " + (r+1) + " Col " + (c+1) + "  $" + pricesOut[r]);
				center.add(seats[r][c]);
			}
		}
		
		//add menu bar
		j.setJMenuBar(menuBar);
		
		//add action listeners for menu
				mExit.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent ae){
						System.exit(1);
					}
				});
				mViewSales.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent ae){
						String out = new String("Total Sales: $" + formatMoney(totalSales));
						JOptionPane.showMessageDialog(null, out,"About",
								JOptionPane.WARNING_MESSAGE);
					}
				});
				mAbout.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent ae){
						JOptionPane.showMessageDialog(null,"Written by: Steve Froehlich -- 2013","About",
								JOptionPane.WARNING_MESSAGE);
					}
				});
				mReset.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent ae){
						JOptionPane.showMessageDialog(j, "Reset all Sales?");
						//change all seats to Green
						for(int r = 0; r < rows; r++){
							for(int c = 0; c < cols; c++){
								seats[r][c].setIcon(green);
							}
						}//end switching all seats to green
						totalSoldSeats = 0;
						currSoldSeats = 0;
						heldSeats.clear();
						//setting text fields to blank
						currSale.setText("Current Sale: $");
						//totalSalesLbl.setText("Total Sales: $" + formatMoney(totalSales));
						soldSeats.setText("Seats Sold: " + totalSoldSeats);
						availSeats.setText("Available Seats: " + ((rows*cols) - totalSoldSeats));
						currSale.setText("Current Sale: $");
						currRowSeat.setText("Row:   Seat:");
						boughtRow.clear();
						
					}
				});
				
				mPurchaseRow.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent ae){
						String enterRow = JOptionPane.showInputDialog("Enter Row Number");
						boolean test = false;
						int in = Integer.parseInt(enterRow);
						System.out.println("size of bought Row " + boughtRow.size());
						for(int s = 0; s < boughtRow.size(); s++){ //checking if a seat in the requested row is taken
							if(in == boughtRow.get(s)){
								test = true;
							}
						}//end for
						if((in <= rows && in > 0)  && (!test) && (enterRow.length() != 0) ){
								//System.out.println("we made it" + in);
								currSoldSeats += cols;
								totalSoldSeats += cols;
								for(int s = 0; s < cols; s++){
									seats[in-1][s].setIcon(red);
									currentSale += prices[in];
								}//end for seats switch
								totalSales += currentSale;
								currSale.setText("Current Sale: $" + formatMoney(currentSale));
								soldSeats.setText("Seats Sold: " + totalSoldSeats);
								availSeats.setText("Available Seats: " + ((rows*cols) - totalSoldSeats));
								boughtRow.add(in);
							}//end if row is a valid row
							else
								JOptionPane.showMessageDialog(null,"Row not available","Error",
										JOptionPane.WARNING_MESSAGE);
						}
				});
		//end menu action listeners
		
		/*-------------------------------------------------------------------
		 * Action Listener for purchasing selected seats
		 * retrieves row number for each seat store in heldSeats and adds the 
		 * price to currentSale
		 * Once complete Total sales is updated current Sale is reset
		 ----------------------------------------------------------------------*/
		purchaseBtn.addActionListener(new ActionListener(){
			int temp;
			public void actionPerformed(ActionEvent ae){
				//totals the sales on the seats clicked
				if(heldSeats.isEmpty())//if no seats have been selected since last purchase
					JOptionPane.showMessageDialog(null,"Select a seat to purchase","CONFLICT",
							JOptionPane.WARNING_MESSAGE);
				for(int i = 0; i < heldSeats.size(); i++){
					temp = heldSeats.get(i);
					currentSale += prices[temp]; //retrieving the  row price form the price array
					boughtRow.add(temp); //adding rows to vector
					System.out.println("adding " + temp);
				}
				totalSales += currentSale;
				currSale.setText("Current Sale: $" + formatMoney(currentSale));
				currSoldSeats = 0;  //resetting current purchase record after display
				currentSale = 0;    //resetting current sold seats
				heldSeats.clear();  //after seats are purchased.
				
				
			}
		});
		
		//action for Purchase from seat text input
		userRowColBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent push){
				//System.out.println("in actionlistener userrowcolbtn");
				//checking input
				if((checkInputInt(userRowText.getText())) && (checkInputInt(userSeatText.getText()))){
					int rowTemp = Integer.parseInt(userRowText.getText());
					int seatTemp = Integer.parseInt(userSeatText.getText());
					//System.out.println("row" + rowTemp);
					//System.out.println("seat" + seatTemp);
					if(((rowTemp <= rows && rowTemp > 0) && (seatTemp > 0 && seatTemp <= cols))){		
							if(seats[rowTemp-1][seatTemp-1].getIcon() == green){
								System.out.println("in green check");
								seats[rowTemp-1][seatTemp-1].setIcon(red);
								currSoldSeats++;
								totalSoldSeats++;
								currentSale =  prices[rowTemp -1];
								totalSales += currentSale;
								currSale.setText("Current Sale: $" + formatMoney(currentSale));
								soldSeats.setText("Seats Sold: " + totalSoldSeats);
								availSeats.setText("Available Seats: " + ((rows*cols) - totalSoldSeats) );
								currentSale = 0;  //resetting current sold seats
								currSoldSeats = 0; //resetting current sold Seat
								userRowText.setText("");
								userSeatText.setText("");
								rowTemp = 0;
								seatTemp = 0;
								boughtRow.add(rowTemp);//adding to purchased row vector
							}//end if seats and rows are within range
					}//end if input is not empty
				}//end if all checks pass
				else{
					//System.out.println("seat reserved");
					JOptionPane.showMessageDialog(null,"Enter a valid seat and row number","CONFLICT",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		
		
		//adding row and seat input fields
		selectPurchase.add(userRow);
		selectPurchase.add(userRowText);
		selectPurchase.add(userSeat);
		selectPurchase.add(userSeatText);
		selectPurchase.add(userRowColBtn);
		
		//setting label sizes
		userRow.setPreferredSize(new Dimension(90, 20));
		userRowText.setPreferredSize(new Dimension(90, 20));
		userSeat.setPreferredSize(new Dimension(90, 20));
		userSeatText.setPreferredSize(new Dimension(90, 20));
		
		//right panel
		right.add(selectPurchase);
		right.add(purchaseBtn);
		right.add(soldSeats);
		right.add(availSeats);
		right.add(currRowSeat);
		right.add(currSale);
		
		//set Button size
		userRowColBtn.setPreferredSize(new Dimension(100, 25));
		
		selectPurchase.setLayout(new FlowLayout());
		right.setLayout(new GridLayout(6, 1));
		center.setLayout(new GridLayout(rows, cols));
		
		//setting panel sizes
		selectPurchase.setPreferredSize(new Dimension(190, 100));
		center.setPreferredSize(new Dimension(900, 550));
		right.setPreferredSize(new Dimension(200, 400));
		right.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		center.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		j.setSize(1100, 650);
		
		right.setBackground(Color.MAGENTA);
		center.setBackground(Color.ORANGE);
		
		//adding panels to main frame
		j.add(right, BorderLayout.EAST);
		j.add(center, BorderLayout.CENTER);
		
		j.setVisible(true);
		
		j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	
	public class MyActionListener implements ActionListener {
	    public void actionPerformed(ActionEvent e) {
	    	int tempRow;
	    	currRowSeat.setText(e.getActionCommand());
	    	JButton temp = (JButton)e.getSource();
	    	tempRow = (int) temp.getClientProperty("Row"); //getting row number
	    	//System.out.println(tempRow);
			if(temp.getIcon() == green){
				heldSeats.add(tempRow); //holding the selected seats until purchase time
				//System.out.println("in green check");
				//System.out.println(tempRow);
				temp.setIcon(red); //swapping seat to sold seat
				totalSoldSeats++;   //update sold seats
				currSoldSeats++;    //updates current selected seats
				soldSeats.setText("Seats Sold: " + totalSoldSeats); //show the total sold seats
				availSeats.setText("Available Seats: " + ((rows*cols) - totalSoldSeats) ); //update available seats
			}
			else{
				if(heldSeats.isEmpty()){    //if the seat has been purchased held seats is empty
					JOptionPane.showMessageDialog(null,"The seat is reserved. Select another seat","CONFLICT",
							JOptionPane.WARNING_MESSAGE);
				}
				else{	
					temp.setIcon(green);  //swapping seat to available
					//System.out.println("in red check");
					//System.out.println(tempRow);
					int idx = heldSeats.indexOf(tempRow);//locate the selected seats row number from vector
					//System.out.println(idx);
					heldSeats.remove(idx); //remove selected seat from vector
					totalSoldSeats--;
					currSoldSeats--;
					soldSeats.setText("Seats Sold: " + totalSoldSeats); //update sold seats
					availSeats.setText("Available Seats: " + ((rows*cols)- totalSoldSeats));//update available seats
				}//end if index is not empty
			}
	        
	    }//end MyActionListener
	   
	}
	
	//formatMoney method
	//passes double and formats to 2 fixed decimal places
	public static String formatMoney(double amt){
		Formatter fmt = new Formatter();
		String fixed = new String(fmt.format("%2.2f", amt).toString());
		fmt.close();
		return fixed;
	}//end formatMoney
			
			
	//returns true is input is a valid integer and not empty
	public static boolean checkInputInt(String check){
		if(check.isEmpty())
			return false;
		char ch;
		for(int i = 0; i < check.length(); i++){
		ch = check.charAt(i);
		//System.out.println(ch);
		if(ch < '0' || ch > '9')
			return false;
		}//end for
		return true;
	}//end checkInputInt method

	
	public static void main(String[] args){
		
		//reading price input
		Scanner scan;
		File file = new File("prices.txt");
		try {
		        scan = new Scanner(file);
		        for(int r = 0; r < rows; r++){
		        	prices[r] = scan.nextDouble();
		        	pricesOut[r] = formatMoney(prices[r]);//formatting prices for output
		        	//System.out.println(prices[r]);
		        }

		} catch (FileNotFoundException e1) {
		            e1.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				new TheatreSeatSales();
			}
		});
	}

}
